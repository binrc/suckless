list:
	@echo avaibale arguments: 
	@grep '^[^#[:space:]].*:' makefile

.FORCE: 


fetch:
	git clone https://git.suckless.org/dwm
	git clone https://git.suckless.org/dmenu
	git clone https://git.suckless.org/slstatus
	git clone https://git.suckless.org/slock
	git clone https://git.suckless.org/st

freebsd: .FORCE
	sudo pkg install pkgconf liberation-fonts-ttf ncurses xautolock
	patch dmenu/config.mk freebsd/freebsd-compile-dmenu.diff
	patch dwm/config.mk freebsd/freebsd-compile-dwm.diff
	patch slock/config.mk freebsd/freebsd-compile-slock.diff
	patch slstatus/config.mk freebsd/freebsd-compile-slstatus.diff
	patch st/config.mk freebsd/freebsd-compile-st.diff

openbsd: .FORCE
	doas pkg_add xautolock colorls
	patch dmenu/config.mk openbsd/dmenu.patch
	patch dwm/config.mk openbsd/dwm.patch
	patch slock/config.mk openbsd/slock.patch
	patch slstatus/config.mk openbsd/slstatus.patch
	patch st/config.mk openbsd/st.patch

netbsd: .FORCE
	su root -c "pkgin install xautolock colorls"
	patch dmenu/config.mk netbsd/dmenu.patch
	patch dwm/config.mk netbsd/dwm.patch
	patch slock/config.mk netbsd/slock.patch
	patch st/config.mk netbsd/st.patch
	echo no slstatus for netbsd

theme: .FORCE
	cd dwm && make config.h && cd ../ && patch dwm/config.h theme/dwm-theme.diff
	cd slock && make config.h && cd ../ && patch slock/config.h theme/slock-theme.diff
	cd slstatus && make config.h && cd ../ && patch slstatus/config.h theme/slstatus-theme.diff
	cd st && make config.h && cd ../ && patch st/config.h theme/st-theme.diff

theme2: .FORCE
	cd dwm && make config.h && cd ../ && patch dwm/config.h theme2/dwm-theme.diff
	cd slock && make config.h && cd ../ && patch slock/config.h theme2/slock-theme.diff
	cd st && make config.h && cd ../ && patch st/config.h theme2/st-theme.diff
	cd slstatus && make config.h && cd ../ && patch slstatus/config.h theme2/slstatus-theme.diff

build: 
	cd dmenu && make
	cd dwm && make
	cd slock && make
	cd st && make
	cd slstatus && make

freeinstall: 
	cd dmenu && sudo make install
	cd dwm && sudo make install
	cd slock && sudo make install
	cd slstatus && sudo make install
	cd st && sudo make install

openinstall: 
	cd dmenu && doas make install
	cd dwm && doas make install
	cd slock && doas make install
	cd slstatus && doas make install
	cd st && doas make install

netinstall: 
	cd dmenu && su root -c "make install"
	cd dwm && su root -c "make install"
	cd slock && su root -c "make install"
	cd st && su root -c "make install"

clean: 
	rm -rf ./dwm ./dmenu ./slstatus ./slock ./st

xsession: .FORCE
	cp dot.xinitrc ~/.xsession
	chmod +x ~/.xsession
	cp dot.xinitrc ~/.xinitrc
	chmod +x ~/.xinitrc

kshconf: .FORCE
	echo 'export ENV=$$HOME/.kshrc' >> ~/.profile
	echo '. ~/.profile' >> ~/.xsession
	echo '. ~/.kshrc' >> ~/.xsession

cshconf: .FORCE
	cp ./dot.cshrc ~/.cshrc

vimconf: .FORCE
	cp ./dot.vimrc ~/.vimrc

tmuxconf: .FORCE
	cp ./dot.tmux.conf ~/.tmux.conf
