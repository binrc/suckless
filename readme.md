# ez build scripts for suckless on various systems


## FreeBSD

For a general freebsd build, use: 

```sh
make fetch freebsd theme build cshconf freeinstall
```

## OpenBSD

For a general openbsd build, use: 

```sh
make fetch openbsd theme build kshconf openinstall
```
## NetBSD

For a general NetBSD build, use: 

```sh
make fetch netbsd theme build netinstall
```

slstatus will fail to build on NetBSD. This is an error you can safely ignore. X might also fail to start using the included xsession because slstatus is nonexistent. 

Additionally, NetBSD requires manual modifications for `cshrc` to work properly. I have not tested much else. 

## Other configs

I have included my (minimal) system configs. Installing these is as simple as 

```sh
make xsession kshconf cshconf vimconf tmuxconf
```

leave out theme if you hate plan 9
