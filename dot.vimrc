set showmatch
filetype on
filetype plugin on
filetype indent on
syntax on
set nocompatible
set ruler

# fix age old BSD backspace bug
set backspace=2
